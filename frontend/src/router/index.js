import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Meja from '@/components/meja'
import MejaForm from '@/components/MejaForm'
import Kursi from '@/components/kursi'
import KursiForm from '@/components/KursiForm'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/meja',
      name: 'Meja',
      component: Meja
    },
    {
      path: '/meja/create',
      name: 'MejaCreate',
      component: MejaForm
    },
    {
      path: '/meja/:id',
      name: 'MejaEdit',
      component: MejaForm
    },
    {
      path: '/kursi',
      name: 'Kursi',
      component: Kursi
    },
    {
      path: '/kursi/create',
      name: 'KursiCreate',
      component: KursiForm
    },
    {
      path: '/kursi/:id',
      name: 'KursiEdit',
      component: KursiForm
    }
  ]
})
