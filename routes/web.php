<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Kursi
$router->post('/kursi','KursiController@create');
$router->get('/kursi','KursiController@read');
$router->post('/kursi/{id}','KursiController@update');
$router->delete('/kursi/{id}','KursiController@delete');
$router->get('kursi/{id}','KursiController@detail');

//Meja
$router->post('/meja','MejaController@create');
$router->get('/meja','MejaController@read');
$router->post('/meja/{id}','MejaController@update');
$router->delete('/meja/{id}','MejaController@delete');
$router->get('meja/{id}','MejaController@detail');