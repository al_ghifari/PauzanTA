<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class meja extends Model
{

  public $table = 't_meja';

  protected $fillable = ['harga','model','stok'];

}
