<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\App;
use App\Kursi;

class KursiController extends Controller
{

  public function create(Request $request)
  {
    $validation = Validator::make($request->all(), [
      'harga' => 'required',
      'model' => 'required',
      'stok' => 'required'
    ]);

    if ($validation->fails()) {
      $error = $validation->errors();
      return [
        'status'=>'error',
        'message'=>$error,
        'result'=>null
      ];
    }

    $result = \App\Kursi::create($request->all());
    if ($result) {
      return [
        'status'=>'success',
        'message'=>'Data sukses ditambahkan',
        'result'=>$result
      ];
    }else {
      return[
        'status'=>'error',
        'message'=>'Data gagal di tambahkan',
        'result'=>null
      ];
    }
  }

public function read(Request $request)
{
  $result = \App\Kursi::all();
  return [
    'status'=>'success',
    'message'=>'',
    'result'=>$result
  ];
}

public function update(Request $request, $id)
{
    $validation = Validator::make($request->all(), [
      'harga' => 'required',
      'model' => 'required',
      'stok' => 'required'
    ]);

    if ($validation->fails()) {
      $errors = $validation->errors();
      return [
        'status'=>'error',
        'message'=>$errors,
        'result'=>null
      ];
    }

    $kursi = \App\Kursi::find($id);
    if (empty($kursi)) {
      return [
        'status'=>'error',
        'message'=>'Data tidak ditemukan',
        'result'=>null
      ];
    }

    $result= $kursi->update($request->all());
    if ($result) {
      return [
        'status'=>'success',
        'message'=>'Data sukses diubah',
        'result'=>$result
      ];
    }else {
      return [
        'status'=>'error',
        'message'=>'Data gagal diubah',
        'result'=>null
      ];
    }
}

public function delete(Request $request, $id)
{

    $kursi = \App\Kursi::find($id);
    if (empty($kursi)) {
      return [
        'status'=>'error',
        'message'=>'Data tidak ditemukan',
        'result'=>null
      ];
    }

    $result= $kursi->delete($id);
    if ($result) {
      return [
        'status'=>'success',
        'message'=>'Data sukses dihapus',
        'result'=>$result
      ];
    }else {
      return [
        'status'=>'error',
        'message'=>'Data gagal dihapus',
        'result'=>null
      ];
    }
}

  public function detail($id)
  {
    $kursi = Kursi::find($id);

    if(empty($kursi)){
      return [
        'status'=>'error',
        'message'=>'Data gagal ditemukan',
        'result'=>null
      ];
    }
      return [
        'status'=>'success',
        'result'=>$kursi
      ];

  }

}



?>
