<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

Class Kursi extends Model
{

  public $table = 't_kursi';

  protected $fillable = ['harga','model','stok'];

}
